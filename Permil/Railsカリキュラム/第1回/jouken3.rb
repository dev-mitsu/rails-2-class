height = 280
weight = 100

if height > 192 && weight > 156
  puts "あなたは、白鵬より大きいです。"
else
  puts "まだまだですね。"
end
# A && B は、A,Bがともにtrueのとき、trueとなる。

if height > 251 || weight > 560
  puts "おめでとう！ギネス認定です！"
end
# A || B は、AもしくはBのどちらかがtrueのときtrueとなる。
