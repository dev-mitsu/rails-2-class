def Vending_machine(money) #自販機メソッドを定義。
  case money

  when "100円"
    return "コーヒー" #return で戻り値を返す

  when "120円"
    return "コーラ"

  when "150円"
    return "お茶"

  end
end

Vending_machine("100円") #これだと出力されない。単なる文字列とみなされる。

puts Vending_machine("100円") #Vending_machineメソッドは文字列を返すので、putsを使って出力できる。


drink = Vending_machine("120円") #もしくは、変数に代入する。
puts drink #これでも出力できる。

#演習①：引数に「ただいま」という文字列を取ると「おかえり」という文字列を返すメソッドを作ろう。
